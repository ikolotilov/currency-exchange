export const CURRENCIES = [
  {
    "code": "USD",
    "name": "US Dollar"
  },
  {
    "code": "CAD",
    "name": "Canadian Dollar"
  },
  {
    "code": "EUR",
    "name": "Euro"
  },
  {
    "code": "AED",
    "name": "United Arab Emirates Dirham"
  },
  {
    "code": "AUD",
    "name": "Australian Dollar"
  },
  {
    "code": "CHF",
    "name": "Swiss Franc"
  },
  {
    "code": "CZK",
    "name": "Czech Republic Koruna"
  },
  {
    "code": "DKK",
    "name": "Danish Krone"
  },
  {
    "code": "GBP",
    "name": "British Pound Sterling"
  },
  {
    "code": "HKD",
    "name": "Hong Kong Dollar"
  },
  {
    "code": "HUF",
    "name": "Hungarian Forint"
  },
  {
    "code": "ILS",
    "name": "Israeli New Sheqel"
  },
  {
    "code": "INR",
    "name": "Indian Rupee"
  },
  {
    "code": "JPY",
    "name": "Japanese Yen"
  },
  {
    "code": "MAD",
    "name": "Moroccan Dirham"
  },
  {
    "code": "NOK",
    "name": "Norwegian Krone"
  },
  {
    "code": "NZD",
    "name": "New Zealand Dollar"
  },
  {
    "code": "PLN",
    "name": "Polish Zloty"
  },
  {
    "code": "QAR",
    "name": "Qatari Rial"
  },
  {
    "code": "RON",
    "name": "Romanian Leu"
  },
  {
    "code": "SEK",
    "name": "Swedish Krona"
  },
  {
    "code": "SGD",
    "name": "Singapore Dollar"
  },
  {
    "code": "THB",
    "name": "Thai Baht"
  },
  {
    "code": "TRY",
    "name": "Turkish Lira"
  },
  {
    "code": "ZAR",
    "name": "South African Rand"
  }
]

export const CURRENCY_SIGNS = {
  "EUR": "€",
  "USD": "$",
  "AUD": "AU$",
  "CZK": "Kč",
  "AED": "AED",
  "CAD": "CA$",
  "CHF": "CHF",
  "DKK": "Dkr",
  "GBP": "£",
  "HKD": "HK$",
  "HUF": "Ft",
  "ILS": "₪",
  "INR": "Rs",
  "JPY": "¥",
  "MAD": "MAD",
  "NOK": "Nkr",
  "NZD": "NZ$",
  "PLN": "zł",
  "QAR": "QR",
  "RON": "RON",
  "SEK": "Skr",
  "SGD": "S$",
  "THB": "฿",
  "TRY": "TL",
  "ZAR": "R"
}

export const RATES_API_URL = 'https://api.fixer.io/latest?base=USD'
