import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import style from './App.scss'
import Header from './components/Header'
import Exchanger from './components/Exchanger'
import CurrencySelect from './components/CurrencySelect'
import stockIcon from './static/images/stock-icon.png'

import { autobind } from 'core-decorators'
import { CURRENCIES } from './constants'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'


import * as exchangeActions from './redux/modules/exchange'
import * as walletsActions from './redux/modules/wallets'

class App extends PureComponent {
  static propTypes = {
    source: PropTypes.string.isRequired,
    destination: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    result: PropTypes.number.isRequired,
    rates: PropTypes.object.isRequired,
    fetching: PropTypes.bool.isRequired,
    wallets: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      changingCurrencyType: null
    }
  }

  @autobind
  handleCurrencyChange(value, type) {
    const { changeExchangeCurrency } = this.props.actions

    changeExchangeCurrency({value, type})

    this.setState({
      changingCurrencyType: null
    })
  }

  @autobind
  handleAmountChange(amount) {
    const { changeExchangeAmount } = this.props.actions;

    if (/^[0-9.]+$/.test(amount) || amount === '') {
      changeExchangeAmount(amount)
    }
  }

  @autobind
  handleExchangeButtonClick(e) {
    const { source, destination, amount, result } = this.props;
    const { changeWalletAmount } = this.props.actions

    changeWalletAmount({ source, destination, amount, result })
  }

  @autobind
  handleCurrencyClick(type) {
    this.setState({
      changingCurrencyType: type
    })
  }

  @autobind
  handleCurrencySelectClose() {
    this.setState({
      changingCurrencyType: null
    })
  }

  componentDidMount() {
    this.props.actions.fetchExchangeRates()
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.rates !== nextProps.rates) {
      clearTimeout(this.pollingTimer);
      if (!nextProps.fetching) {
        this.startPolling();
      }
    }
  }

  componentWillUnmount() {
      clearTimeout(this.pollingTimer);
  }

  @autobind
  startPolling() {
    this.pollingTimer = setTimeout(
      () => this.props.actions.fetchExchangeRates(),
    10000);
  }

  renderStockIcon() {
    return <img src={ stockIcon } className={ style.StockButtonImage } alt=""/>
  }

  render() {
    const {
      source,
      destination,
      amount,
      result,
      rates,
      wallets,
    } = this.props

    const { changingCurrencyType } = this.state

    const rate =  1 / rates[source] * rates[destination];
    const selectedCurrency = this.props[changingCurrencyType]
    const currencies = CURRENCIES

    return (
      <div className={ style.Container }>
        <div className={ style.Wrapper }>
          <Header
            heading={ 'Exchange' }
            withCloseButton={ true }
            renderRightSideComponent={ this.renderStockIcon } />
          <Exchanger
            { ...{ source, destination, amount, result, rate, wallets } }
            onExchangeButtonClick={ this.handleExchangeButtonClick}
            onCurrencyClick={ this.handleCurrencyClick }
            onAmountChange={ this.handleAmountChange }
          />
          <CurrencySelect
            { ...{ currencies, wallets, selectedCurrency } }
            type={ changingCurrencyType }
            onClose={ this.handleCurrencySelectClose }
            onChange={ this.handleCurrencyChange }
          />
      </div>

      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
      ...exchangeActions,
      ...walletsActions
  }, dispatch)
})

const mapStateToProps = state => ({
    ...state.exchange,
    wallets: state.wallets
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
