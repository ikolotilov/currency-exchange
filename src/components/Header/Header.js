import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import style from './Header.scss'
import { X, Search, ArrowLeft } from 'react-feather'
import { autobind } from 'core-decorators'
import cn from 'classnames'

export default class Header extends PureComponent {

  static propTypes = {
    heading: PropTypes.string.isRequired,
    className: PropTypes.string,
    withCloseButton: PropTypes.bool,
    withBackButton: PropTypes.bool,
    withSearch: PropTypes.bool,
    isSearchActive: PropTypes.bool,
    renderRightSideComponent: PropTypes.func,
    onBackButtonClick: PropTypes.func,
  }

  @autobind
  handleSearchQueryChange(e) {
    const value = e.target.value
    const { onQueryChange } = this.props
    onQueryChange(value)
  }

  @autobind
  handleSearchButtonClick() {
    const { onSearchButtonClick, onQueryChange } = this.props
    onQueryChange(null)
    onSearchButtonClick()
    setTimeout(()=>{
      this.focusSearchInput()
    }, 100)
  }

  @autobind
  focusSearchInput() {
    this.searchInput && this.searchInput.focus()
  }

  render() {
    const {
      heading,
      withCloseButton,

      withBackButton,
      onBackButtonClick,

      withSearch,
      isSearchActive,

      renderRightSideComponent,
      className
    } = this.props

    return (
        <header className={ cn(style.Wrapper, {[className]: className}) }>
          {
            withCloseButton &&  <X className={ style.CloseButton }/>
          }
          {
            withBackButton && <ArrowLeft
              className={ style.BackButton }
              onClick={ onBackButtonClick }
            />
          }
          <div className={ style.InnerContainer }>
            {
              isSearchActive
              ? <input
                type="text"
                className={ style.SearchInput }
                onChange={ this.handleSearchQueryChange }
                placeholder={ 'Type currency here' }
                ref={ (element) => {
                  this.searchInput = element
                } }
              />
              : heading
            }
          </div>
          {
            renderRightSideComponent && renderRightSideComponent()
          }
          {
            withSearch
              ? <div>
                {
                  isSearchActive
                  ? <X
                    className={ style.CloseButton }
                    onClick={ this.handleSearchButtonClick }
                  />
                  : <Search
                    className={ style.SearchButton }
                    onClick={ this.handleSearchButtonClick }
                  />
                }
              </div>
              : null
          }
        </header>
    );
  }
}
