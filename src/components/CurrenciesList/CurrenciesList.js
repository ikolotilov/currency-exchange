import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { autobind } from 'core-decorators'
import style from './CurrenciesList.scss'
import cn from 'classnames'

export default class CurrenciesList extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    currencies: PropTypes.array,
    listItemProps: PropTypes.object,
    wallets: PropTypes.object,
    selectedCurrency: PropTypes.string
  }

  @autobind
  handleListItemClick(e) {
    const { onChange, type } = this.props
    onChange(e.target.value, type)
  }

  render() {
    const {
      className,
      currencies,
      listItem,
      listItemProps,
      wallets,
      selectedCurrency
    } = this.props

    return (
      <div className={ cn(style.Wrapper, { [className]: className }) }>
        {
          currencies && currencies.map(currency => {
            let ListItem = listItem
            let amount = wallets[currency.code] && wallets[currency.code].amount
            let data = {
              ...currency,
              amount
            }

            return (
              <ListItem
                data={ data }
                key={ currency.code }
                onChange={ this.handleListItemClick }
                selected={ currency.code === selectedCurrency }
                { ...listItemProps }
              />
            )
          })
        }
      </div>
    )
  }
}
