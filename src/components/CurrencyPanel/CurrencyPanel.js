import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import style from './CurrencyPanel.scss'
import cn from 'classnames'
import { autobind } from 'core-decorators'
import accounting from 'accounting'
import { ChevronDown } from 'react-feather'
import CurrencyInput from '../CurrencyInput'
import { CURRENCY_SIGNS } from '../../constants'

export default class CurrencyPanel extends PureComponent {
  static propTypes = {
    code: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    type: PropTypes.string.isRequired,
    wallet: PropTypes.object,
    onChange: PropTypes.func
  }

  @autobind
  handleClick() {
    const { onClick, type } = this.props
    onClick(type)
  }

  format(value) {
    return accounting.formatNumber(value, 2, ' ', ',').replace(/,?00+$/, '')
  }

  render() {
    let { code, amount, type, wallet, onChange } = this.props

    let preparedAmount = this.format(amount)
    let walletBalance = wallet && wallet.amount && this.format(wallet.amount)

    let isAmountPositive = amount > 0
    let sign = isAmountPositive
      ? type === 'destination' ? '+' : '–'
      : null

    let currencySign = CURRENCY_SIGNS[code]
    return (
      <div className={ cn(style.Wrapper, { [style.Wrapper__grey]: type === 'destination'} ) }>
        <div className={ style.CurrencyContainer }>
          {
            <div className={ style.CurrencySelect } onClick={ this.handleClick }>
              { code }
              <ChevronDown size={15} />
            </div>
          }
          <div className={ style.Display }>
            {
              type === 'destination'
              ? <span className={ cn({[style.Dimmed]: !isAmountPositive}) }>
                { sign } { preparedAmount }
              </span>
              : <div className={ style.InputContainer }>
                <span>{ sign }</span>
                <CurrencyInput
                  onChange={ onChange }
                  value={ amount }
                  className={ cn(style.Input, { [style.Dimmed]: !isAmountPositive} ) }
                  id="autoresizing"
                />
              </div>
            }
          </div>
        </div>
        <div className={ style.Wallet }>Balance: { walletBalance || 0 } { currencySign }</div>
      </div>
    )
  }
}
