import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import style from './Modal.scss'
import { autobind } from 'core-decorators'

export default class Modal extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func,
    isShown: PropTypes.bool,
    heading: PropTypes.string
  }

  @autobind
  handleClose(e) {
    if (!e.target.closest('[data-modal-wrapper]'))
     this.props.handleClose()
  }

  render() {
    if (!this.props.isShown) {
      return null
    }

    const { heading } = this.props

    return (
      <div className={ style.Overlay } onClick={ this.handleClose } data-modal-overlay>
        <div className={ style.Wrapper } data-modal-wrapper>
          {
            heading
             ? <div className={ style.Heading }>{ heading }</div>
             : null
          }
          { this.props.children }
        </div>
      </div>
    )
  }
}
