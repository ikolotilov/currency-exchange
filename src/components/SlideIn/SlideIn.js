import React, { PureComponent } from 'react'
import style from './SlideIn.scss'
import { Transition } from 'react-transition-group'
import cn from 'classnames'

export default class SlideIn extends PureComponent {

  render() {
    return <Transition
      in={ this.props.in }
      timeout={ {enter: 100, exit: 400} }
      unmountOnExit={true}>
        {
          (state) => {
            let { direction } = this.props
            return (
            <div className={ cn(
              style.Wrapper,
              style[`Wrapper__${direction}`],
              style[`Wrapper__${state}`]
              ) }>
              { this.props.children }
            </div>
          )}
        }
      </Transition>
  }
}
