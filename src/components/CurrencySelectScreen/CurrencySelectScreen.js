import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import style from './CurrencySelectScreen.scss'
import Header from '../Header'
import CurrenciesList from '../CurrenciesList'
import CurrenciesListItem from '../CurrenciesListItem'
import { autobind } from 'core-decorators'

export default class CurrencySelectScreen extends PureComponent {
  static propTypes = {
    currencies: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedCurrency: PropTypes.string,
    currenciesWithWallets: PropTypes.arrayOf(PropTypes.object),
    wallets: PropTypes.object.isRequired,
    type: PropTypes.string,

    onChange: PropTypes.func.isRequired,
    onBackButtonClick: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      isSearchActive: false,
      searchQuery: null
    }
  }

  @autobind
  handleQueryChange(value) {
    this.setState({
      searchQuery: value
    })
  }

  @autobind
  handleSearchButtonClick() {
    this.setState({
      isSearchActive: !this.state.isSearchActive
    })
  }

  render() {
    const {
      currencies,
      selectedCurrency,
      currenciesWithWallets,
      wallets,
      type,

      onChange,
      onBackButtonClick
    } = this.props

    const { isSearchActive, searchQuery } = this.state

    let otherCurrencies = currencies
      .filter(item => !currenciesWithWallets
        .find(i => i.name === item.name))

    let isSearchingInProgress = isSearchActive
      && searchQuery
      && searchQuery !== ''

    let filteredCurrencies
    if (isSearchingInProgress) {
      filteredCurrencies = currencies.filter(item => {
        let re = new RegExp(`(^|\\s)${ searchQuery }`, 'i')
        return re.test(item.name)
        || item.code.indexOf(searchQuery.toUpperCase()) !== -1
      })
    }

    return (
      <div className={ style.Wrapper }>
        <Header
          withSearch={ true }
          withBackButton={ true }
          onBackButtonClick={ onBackButtonClick }
          onSearchButtonClick={ this.handleSearchButtonClick }
          onQueryChange={ this.handleQueryChange }
          isSearchActive={ isSearchActive }
          heading={ 'Choose currency' }
          className={ style.Header }
        />
          {
            isSearchingInProgress
            ? <div className={ style.InnerContainer }>
                <CurrenciesList
                  {...{ selectedCurrency, wallets, onChange, type } }
                  className={ style.RecentlyUsedList }
                  currencies={ filteredCurrencies }
                  listItem={ CurrenciesListItem }
                  listItemProps={ { isExtended: true } }
                />
              </div>
            : <div className={ style.InnerContainer }>
                <div className={ style.Heading }>
                Recently used
                </div>
                <CurrenciesList
                  {...{ type, selectedCurrency, wallets, onChange, }}
                  className={ style.RecentlyUsedList }
                  currencies={ currenciesWithWallets }
                  listItem={ CurrenciesListItem }
                  listItemProps={{ isExtended: true }}
                />
                <div className={ style.Heading }>
                  Other currencies
                </div>
                <CurrenciesList
                  { ...{ type, selectedCurrency, wallets, onChange } }
                  currencies={ otherCurrencies }
                  listItem={ CurrenciesListItem }
                  listItemProps={{ isExtended: true }}
                />
              </div>
          }
      </div>
    )
  }
}
