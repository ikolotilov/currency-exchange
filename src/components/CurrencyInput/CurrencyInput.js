import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import AutosizeInput from 'react-input-autosize'
import { autobind } from 'core-decorators'

export default class CurrencyInput extends PureComponent {
  static propTypes = {
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    className: PropTypes.string,
    onChange: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)


    this.state = {
      value: props.value || 0,
      formattedValue: this.format(props.value || 0)
    }

    this.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
  }

  updateValueIfNeeded(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({
        value: this.unformat(nextProps.value),
        formattedValue: this.format(nextProps.value)
      })
    }
  }

  @autobind
  handleChange(e) {
    let inputValue = e.target.value
    if (inputValue === '' || +inputValue === 0) {
      inputValue = '0'
    } else {
      if (!(/^[0-9.,\s]+$/.test(inputValue))) {
        return
      }

      let parts = inputValue.split(',')
      if (parts[1] && parts[1].length > 2) {
        return
      }

      let commasCount = (inputValue.match(/,/g) || []).length
      let dotsCount = (inputValue.match(/\./g) || []).length

      if (commasCount || dotsCount) {
        if (inputValue.length > 13) {
          return
        }
      } else if (inputValue.length > 10){
        return
      }

      if (commasCount + dotsCount > 1) {
        return
      }
    }

    let newValue = this.unformat(inputValue)
    let newState = {
      value: newValue,
      formattedValue: this.format(inputValue)
    }

    this.setState(newState)
    this.props.onChange(newValue)
  }

  format(inputValue) {
    let formatter = (value) => {

      let inputValueToCheck = typeof value === 'string'
        ? value.replace(new RegExp(',', 'g'), '.').replace(new RegExp(' ', 'g'), '')
        : value

      if (+inputValueToCheck % 1 === 0) {
        return value
          .toString()
          .replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
          .replace('.', ',')

      } else {
        return value
          .toLocaleString()
          .replace('.', ',')
      }
    }

    return formatter(inputValue)
  }

  unformat(value) {
    return +(value.toString()
      .replace(/\s/g, '')
      .replace(new RegExp(',', 'g'), '.'))
  }

  @autobind
  handleKeyUp(e) {
    if (e.keyCode === 32) {
      e.preventDefault();
      e.stopPropagation();
      return false;
    }
  }

  @autobind
  handleFocus(e) {
    let { value } = e.target
    if (value && value === '0') {
      e.target.select()
    }
  }

  componentWillReceiveProps(nextProps) {
    this.updateValueIfNeeded(nextProps)
  }

  render() {
    const { className } = this.props

    const { formattedValue } = this.state
    return (
      <div className={ className }>
        <AutosizeInput
          value={ formattedValue }
          ref={ (input) => { this.input = input } }
          onChange={ this.handleChange }
          onFocus={ this.handleFocus }
          onKeyDown={ this.handleKeyUp }
          data-fixed={ this.isSafari }
        />
      </div>
    )
  }
}
