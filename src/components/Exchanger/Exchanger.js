import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import style from './Exchanger.scss'
import CurrencyPanel from '../CurrencyPanel'
import { TrendingUp } from 'react-feather'
import { CURRENCY_SIGNS } from '../../constants'
import accounting from 'accounting'

export default class Exchanger extends PureComponent {
  static propTypes = {
    source: PropTypes.string.isRequired,
    destination: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    result: PropTypes.number.isRequired,
    rate: PropTypes.number.isRequired,
    wallets: PropTypes.object.isRequired,

    onCurrencyClick: PropTypes.func.isRequired,
    onAmountChange: PropTypes.func.isRequired,
    onExchangeButtonClick: PropTypes.func.isRequired
  }

  render() {
    const {
      source,
      destination,
      amount,
      result,
      rate,
      wallets,

      onCurrencyClick,
      onAmountChange,
      onExchangeButtonClick
    } = this.props;

    const sourсeCurrencyAmount = wallets[source] && wallets[source].amount
    const sourceSign = CURRENCY_SIGNS[source]
    const destinationSign = CURRENCY_SIGNS[destination]
    const isExchageButtonActive = amount > 0 && sourсeCurrencyAmount >= amount
    const formattedRate = accounting.formatNumber(rate, 4, ' ', ',')

    return (
      <div className={ style.Wrapper }>
        <CurrencyPanel
          type="source"
          code={ source }
          amount={ amount }
          onClick={ onCurrencyClick }
          onChange={ onAmountChange }
          wallet={ wallets[source] }
        />
        <div className={ style.CurrencyRateDisplayContainer }>
          {
            rate
            ? <div className={ style.CurrencyRateDisplay }>
              <TrendingUp size={ 13 } color='#3289ee'/>
              1,00 { sourceSign } = { formattedRate } { destinationSign }
            </div>
            : null
          }

        </div>
        <CurrencyPanel
          type="destination"
          code={ destination }
          amount={ result }
          onClick={ onCurrencyClick }
          wallet={ wallets[destination] }
        />
        <div className={ style.ButtonContainer }>
          <button
            className={ style.Button }
            disabled={ !isExchageButtonActive }
            onClick={ onExchangeButtonClick }>
            Exchange
          </button>
        </div>

      </div>
    );
  }
};
