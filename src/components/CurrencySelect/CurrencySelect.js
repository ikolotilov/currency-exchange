import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Modal from '../Modal'
import CurrencySelectView from '../CurrencySelectView'
import CurrencySelectScreen from '../CurrencySelectScreen'
import SlideIn from '../SlideIn'
import { autobind } from 'core-decorators'

export default class CurrencySelect extends PureComponent {
  static propTypes = {
    wallets: PropTypes.object.isRequired,
    type: PropTypes.string,
    currencies: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedCurrency: PropTypes.string,
    onChange: PropTypes.func.isRequired
  }
  
  constructor(props) {
    super(props)

    this.state = {
      isExpanded: false
    }
  }

  @autobind
  handleExpand() {
    this.setState({
      isExpanded: true
    })
  }

  @autobind
  handleClose() {
    this.setState({
      isExpanded: false
    })

    this.props.onClose()
  }

  @autobind
  handleChange(value, type) {
    this.setState({
      isExpanded: false
    })

    this.props.onChange(value, type)
  }

  render() {
    const {
      wallets,
      type,
      currencies,
      selectedCurrency,

      onChange
    } = this.props

    const currenciesWithWallets = Object.keys(wallets)
      .filter(item => wallets[item].active)
        .map(item => currencies.find(i => i.code === item))

    const { isExpanded } = this.state

    return (
      <div>
        <SlideIn in={ type && isExpanded } direction='left'>
            <CurrencySelectScreen
            { ...{
              currencies,
              selectedCurrency,
              type,
              wallets,
              currenciesWithWallets
            }}
            onChange={ this.handleChange }
            onBackButtonClick={ this.handleClose }
          />
        </SlideIn>
        <SlideIn in={ Boolean(type) } direction='up'>
          <Modal
              isShown={ true }
              heading="Choose currency"
              handleClose={ this.handleClose }>
              <CurrencySelectView
                { ...{ selectedCurrency, wallets, type } }
                currencies={ currenciesWithWallets }
                onChange={ onChange }
                onExpand={ this.handleExpand }
              />
            </Modal>
        </SlideIn>
      </div>
    )
  }
}
