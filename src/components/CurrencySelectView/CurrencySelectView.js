import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import CurrenciesList from '../CurrenciesList'
import style from './CurrencySelectView.scss'
import { Globe } from 'react-feather'
import { autobind } from 'core-decorators'
import CurrenciesListItem from '../CurrenciesListItem'

export default class CurrencySelectView extends PureComponent {
  static propTypes = {
    type: PropTypes.string,
    wallets: PropTypes.object.isRequired,
    currencies: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedCurrency: PropTypes.string,

    onChange: PropTypes.func.isRequired,
    onExpand: PropTypes.func.isRequired
  }

  @autobind
  handleCurrencyChange(code) {
    const { onChange, currencyType } = this.props
    onChange(code, currencyType)
  }

  render() {
    const {
      type,
      wallets,
      currencies,
      selectedCurrency,

      onChange,
      onExpand
    } = this.props

    const firstThreeCurrencies = currencies.slice(0, 3)

    return (
      <div className={ style.Wrapper }>
        <CurrenciesList
          { ...{ selectedCurrency, wallets, type, onChange } }
          currencies={ firstThreeCurrencies }
          listItem={ CurrenciesListItem }
        />
        <div
          className={ style.OtherCurrenciesButton }
          onClick={ onExpand }>
          <Globe size={ 20 } color={ '#3289ee' }/>
          Other
        </div>
      </div>
    )
  }
}
