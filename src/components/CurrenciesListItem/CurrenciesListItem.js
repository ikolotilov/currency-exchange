import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import style from './CurrenciesListItem.scss'
import cn from 'classnames'
import accounting from 'accounting'
import { Check } from 'react-feather'

export default class CurrenciesListItem extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,
    selected: PropTypes.bool,
    onChange: PropTypes.func.isRequired
  }

  render() {
    const { code, amount, name } = this.props.data
    const { isExtended, selected, onChange } = this.props

    let currencyFlagClassName = 'CurrencyFlag__' + code.toLowerCase()
    let preparedAmount = amount
      ? ` · ${ accounting.formatNumber(amount, 2, ' ', ',').replace(/,?00+$/, '') }`
      : null

    return (
      <div className={ cn(style.Wrapper, { [style.Extended]: isExtended }) } key={ code }>
        <input id={ code }
          name='currency'
          value={ code }
          onChange={ onChange }
          type="radio"
          checked={ selected } />
        <label htmlFor={ code }>
          <div className={ cn(style.CurrencyFlag, style[currencyFlagClassName]) }></div>
            {
              isExtended
              ? <div className={ style.TextContainer }>
                  <div className={ cn(style.InnerContainerVertical, { [style.Active]: selected }) }>
                    <div className={ style.Code }>
                      { code }
                      { preparedAmount }
                    </div>
                    <div className={ style.Name }>
                      { name }
                    </div>
                  </div>
                  {
                    selected
                    ? <Check color={ '#3289ee' } size={ 20 }/>
                    : null
                  }
              </div>
              : <div className={ style.TextContainer }>
                  <div className={ cn(style.InnerContainer, { [style.Active]: selected }) }>
                    { code }
                    { preparedAmount }
                  </div>
                  {
                    selected
                    ? <Check color={ '#3289ee' } size={ 20 }/>
                    : null
                  }
              </div>
            }
        </label>
      </div>
    )
  }
}
