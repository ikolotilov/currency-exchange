const WALLET_AMOUNT_CHANGE = 'revolut-exchange/wallets/WALLET_AMOUNT_CHANGE'

const initialState = {
  'CHF': {
    amount: 1000,
    active: true
  },
  'EUR': {
    amount: 9000,
    active: true
  },
  'CZK': {
    amount: 700,
    active: true
  }
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case WALLET_AMOUNT_CHANGE:
      const { source, destination, amount, result } = action.payload

      if (state[destination]) {
        return {
            ...state,
            [source]: {
              ...state[source],
              amount: state[source].amount - amount
            },
            [destination]: {
              ...state[destination],
              amount: state[destination].amount + result
            }
          }
      } else {
        return {
          ...state,
          [source]: {
            ...state[source],
            amount: state[source].amount - amount
          },
          [destination]: {
            amount: result,
            active: true
          }
        }
      }

    default:
      return state
  }
}
export function changeWalletAmount(payload) {
  return {
    type: WALLET_AMOUNT_CHANGE,
    payload
  }
}
