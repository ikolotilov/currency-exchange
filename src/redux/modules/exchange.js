import { RATES_API_URL } from '../../constants'

const EXCHANGE_CURRENCY_CHANGE = 'revolut-exchange/exchange/EXCHANGE_CURRENCY_CHANGE'
const EXCHANGE_AMOUNT_CHANGE = 'revolut-exchange/exchange/EXCHANGE_AMOUNT_CHANGE'

const EXCHANGE_RATES_FETCH_REQUEST = 'revolut-exchange/exchange/EXCHANGE_RATES_FETCH_REQUEST'
const EXCHANGE_RATES_FETCH_SUCCESS = 'revolut-exchange/exchange/EXCHANGE_RATES_FETCH_SUCCESS'
const EXCHANGE_RATES_FETCH_FAIL = 'revolut-exchange/exchange/EXCHANGE_RATES_FETCH_FAIL'

const initialState = {
  source: 'CHF',
  destination: 'EUR',
  amount: 0,
  result: 0,
  rates: {},
  fetching: false
}

export default function reducer(state = initialState, action = {}) {
  let { source, destination, amount, rates  } = state

  switch (action.type) {
    case EXCHANGE_CURRENCY_CHANGE:
      let { value, type } = action.payload
      let oppositeType = type === 'source'
        ? 'destination'
        : 'source'
      let rate

      if (state[oppositeType] === value) {
        rate = 1 / rates[destination] * rates[source]

        return {
          ...state,
          source: destination,
          destination: source,
          result: amount * rate,
        }
      }

      if (type === 'source') {
        rate = 1 / rates[value] * rates[destination]
      } else {
        rate = 1 / rates[source] * rates[value]
      }

      return {
        ...state,
        [type]: value,
        result: amount * rate,
      }

    case EXCHANGE_AMOUNT_CHANGE:
      return {
        ...state,
        amount: action.payload,
        result: action.payload * 1 / rates[source] * rates[destination]
      }

    case EXCHANGE_RATES_FETCH_REQUEST:
      return {
        ...state,
        fetching: true
      }

    case EXCHANGE_RATES_FETCH_SUCCESS:
      let newRates = { ...action.payload.rates }
      // use mockChanges function for mocking EUR rate changes
      // example:
      //
      // let newRates = { ...mockChanges(action.payload).rates }
      return {
        ...state,
        rates: newRates,
        result: amount * 1 / newRates[source] * newRates[destination],
        fetching: false
      }

    case EXCHANGE_RATES_FETCH_FAIL:
      return {
        ...state,
        fetching: false
      }

    default:
      return state;
  }
}

export function changeExchangeCurrency(code) {
  return {
    type: EXCHANGE_CURRENCY_CHANGE,
    payload: code
  }
}

export function changeExchangeAmount(value) {
  return {
    type: EXCHANGE_AMOUNT_CHANGE,
    payload: value
  }
}

function mockChanges(data) {
  return {
    ...data,
    rates: {
      ...data.rates,
      EUR: data.rates.EUR + Math.random()/30
    }
  }
}

function fetchExchangeRatesRequest() {
  return {
    type: EXCHANGE_RATES_FETCH_REQUEST
  }
}

function fetchExchangeRatesSuccess(payload) {
  return {
    type: EXCHANGE_RATES_FETCH_SUCCESS,
    payload
   }
}

function fetchExchangeRatesFail() {
  return {
    type: EXCHANGE_RATES_FETCH_FAIL
  }
}

function fetchExchangeRatesAsync() {
  return new Promise((resolve, reject) => {
    fetch(RATES_API_URL)
      .then((result) => {
        resolve(result.json())
      })
      .catch((e) => {
        reject(e)
      })
  })
}

export const fetchExchangeRates = (payload) => async (dispatch) => {
  dispatch(fetchExchangeRatesRequest())

  try {
    let result = await fetchExchangeRatesAsync()
    dispatch(fetchExchangeRatesSuccess(result))
  } catch (e) {
    console.log('An error occured', e)
    dispatch(fetchExchangeRatesFail())
  }
}
